﻿using System.Collections.Generic;
using UnityEngine;
using Pathing;
using Talespin.Tween;

public enum NodeType
{
    Grass,
    Desert,
    Mountain,
    Forest,
    Water
}

public enum HighlightType
{
    Selection,
    Path
}

namespace Talespin.Generic
{
    [RequireComponent(typeof(TweenPosition))]
    public class Node : MonoBehaviour, IAStarNode
    {
        public struct GridCoordinates
        {
            public int row;
            public int column;

            public GridCoordinates(int p_row, int p_column)
            {
                this.row = p_row;
                this.column = p_column;
            }
        }

        private List<IAStarNode> _neighboursNodes = new List<IAStarNode>();

        [SerializeField]
        private NodeType _nodeType;

        private MeshRenderer _rend;
        private TweenPosition _tweenPos;

        private Texture _texture;
        private Color _color;

        public GridCoordinates coordinates;

        [SerializeField]
        private GameObject _model;

        #region Pathfinding

        private Node _target;

        #endregion

        #region Getters

        public IEnumerable<IAStarNode> Neighbours => this._neighboursNodes;

        public float cost => ProjectManager.Instance.nodesData.GetCost(this._nodeType);

        public bool canBeRouted => this.cost >= 0;

        public float CostTo(IAStarNode neighbour)
        {
            return ((Node)neighbour).cost;
        }

        public float EstimatedCostTo(IAStarNode goal)
        {
            this._target = (Node)goal;
            return BFS.EstimatedCost(this, this._target);
        }

        #endregion

        private void Awake()
        {
            this._rend = this._model.GetComponent<MeshRenderer>();
            this._tweenPos = this.GetComponent<TweenPosition>();
        }

        private void Start()
        {
            this._tweenPos.SetTween(this.transform.localPosition, 
                                    new Vector3(this.transform.localPosition.x,
                                                ProjectManager.Instance.nodesData.selectionYOffset,
                                                this.transform.localPosition.z),
                                    -1f, true);            
        }

        public void SetUp(NodeType p_nodeType)
        {
            this._nodeType = p_nodeType;

            this.SetTexture();
            this.ToggleHighlight(false);
        }

        public void SetCoordinates(int p_row, int p_column)
        {
            this.coordinates = new GridCoordinates(p_row, p_column);
        }

        public void GetNeighbours()
        {
            GridCoordinates _coordinates;

            for (int i = this.coordinates.row - 1; i <= this.coordinates.row + 1; i++)
            {
                for (int j = this.coordinates.column - 1; j <= this.coordinates.column + 1; j++)
                {
                    if(i != this.coordinates.row &&
                      ((i + 1) % 2 != 0 && j == this.coordinates.column - 1 ||
                      ((i + 1) % 2 == 0 && j == this.coordinates.column + 1)))
                    {
                        continue;
                    }

                    _coordinates.row = i;
                    _coordinates.column = j;

                    this.AddNeighbour(Grid.Instance.GetNode(_coordinates));
                }
            }
        }

        private void AddNeighbour(Node p_node)
        {
            if (p_node != null && p_node != this && p_node.canBeRouted)
            {
                this._neighboursNodes.Add(p_node);
            }
        }

        [ContextMenu("Set Texture")]
        public void SetTexture()
        {
            this._texture = ProjectManager.Instance.nodesData.GetTexture(this._nodeType);
            this._rend.material.mainTexture = this._texture;
        }

        public void OnSelection()
        {
            if (UIManager.CurrentScreen.screenType == ScreenType.MainScreen && ProjectManager.Path == null && this._nodeType != NodeType.Water)
            {
                ProjectManager.Instance.CheckPathMargin(PathMarginType.Start, this, () =>
                {
                    ProjectManager.Instance.CheckPathMargin(PathMarginType.Target, this);
                });
            }

            UIManager.Instance.GetScreenScript<MainScreen>().CheckElements(false);
        }

        public void ToggleHighlight(bool p_enable, HighlightType p_type = default)
        {
            if (p_enable)
            {
                if (this.transform.position.y == 0)
                {
                    this._tweenPos.PlayForward();
                }

                switch (p_type)
                {
                    case HighlightType.Selection:

                        this._color = ProjectManager.Instance.nodesData.selectionColor;
                        break;

                    case HighlightType.Path:

                        this._color = ProjectManager.Instance.nodesData.pathColor;
                        break;

                    default:

                        Debug.LogError("Missing HighlightType parameter");
                        break;
                }
            }
            else
            {
                if (this.transform.position.y > 0)
                {
                    this._tweenPos.PlayReverse();
                }

                this._color = Color.white;
            }

            this._rend.material.color = this._color;
        }
    }
}
