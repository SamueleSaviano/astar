﻿using System;
using UnityEngine;

namespace Talespin.Generic
{
    public class Grid : MonoBehaviour
    {
        public static Grid Instance;

        #region GridCreation

        private Array _nodeTypes;

        private GameObject _nodeClone;
        private Node _node;

        private Vector3 _startPosition;
        private Vector3 _nodePosition;
        private Vector3 _nodeRotation;

        private int _rand;
        private float _nodeScale;
        private float _rowOffset;

        #endregion

        private Node[,] _nodes;

        public Node GetNode(Node.GridCoordinates p_coordinates)
        {
            if(p_coordinates.row >= 0 && p_coordinates.row < ProjectManager.Instance.gridData.rows &&
               p_coordinates.column >= 0 && p_coordinates.column < ProjectManager.Instance.gridData.columns)
            {
                return this._nodes[p_coordinates.row, p_coordinates.column];
            }

            return null;
        }

        private void Awake()
        {
            Grid.Instance = this;

            this._nodeTypes = Enum.GetValues(typeof(NodeType));            
        }

        public void SetNode(Node p_node)
        {
            this._nodes[p_node.coordinates.row, p_node.coordinates.column] = p_node;
        }

        public void CreateGrid()
        {
            if (this._nodes != null)
            {
                return;
            }

            this._nodes = new Node[ProjectManager.Instance.gridData.rows, ProjectManager.Instance.gridData.columns];

            this._startPosition = Vector3.zero;
            this._nodePosition = Vector3.zero;
            this._nodeRotation = Vector3.zero;

            this._nodeScale = 1f;

            this._startPosition.x = ProjectManager.Instance.gridData.columns * 0.5f - this._nodeScale * 0.5f;
            this._startPosition.z = -ProjectManager.Instance.gridData.rows * 0.5f + this._nodeScale;
            this._nodeRotation.x = -90f;

            for (int _row = 0; _row < ProjectManager.Instance.gridData.rows; _row++)
            {
                this._rowOffset = ((_row + 1) % 2 == 0) ? this._nodeScale * 0.5f : 0;

                for (int _column = 0; _column < ProjectManager.Instance.gridData.columns; _column++)
                {
                    this._nodePosition.x = this._startPosition.x - (this._nodeScale * _column) - this._rowOffset;
                    this._nodePosition.z = this._startPosition.z + (this._nodeScale.Percentage(75f) * _row);

                    this.CreateNode(this._nodePosition, _row, _column);
                }
            }

            for (int i = 0; i < ProjectManager.Instance.gridData.rows; i++)
            {
                for (int j = 0; j < ProjectManager.Instance.gridData.columns; j++)
                {
                    this._nodes[i, j].GetNeighbours();
                }
            }

            this.transform.position = new Vector3(this._nodeScale * 0.5f, 0, 0);
        }

        private void CreateNode(Vector3 p_pos, int p_row, int p_col)
        {
            this._rand = UnityEngine.Random.Range(0, this._nodeTypes.Length);

            this._nodeClone = GameObject.Instantiate(ProjectManager.Instance.nodesData.prefab, p_pos, Quaternion.Euler(this._nodeRotation), this.transform);
            this._nodeClone.transform.localScale = Vector3.one * this._nodeScale;
            this._nodeClone.name = $"Node_R{p_row}_C{p_col}";

            this._node = this._nodeClone.GetComponent<Node>();
            this._node.SetUp((NodeType)this._nodeTypes.GetValue(this._rand));
            this._node.SetCoordinates(p_row, p_col);

            this.SetNode(this._node);
        }

        public void ResetGrid()
        {
            for (int i = 0; i < ProjectManager.Instance.gridData.rows; i++)
            {
                for (int j = 0; j < ProjectManager.Instance.gridData.columns; j++)
                {
                    GameObject.Destroy(this._nodes[i, j].gameObject);
                }
            }

            this._nodes = null;
            this.transform.position = Vector3.zero;

            this.CreateGrid();
        }
    }
}
