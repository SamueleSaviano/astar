﻿using Talespin.UI;

public class IntroScreen : ProjScreen
{
    public override void EnterScreen()
    {
        base.EnterScreen();
    }

    public override void ExitScreen()
    {
        base.ExitScreen();
    }

    public void PlayButton()
    {
        UIManager.Instance.ChangeScreen(ScreenType.MainScreen);
    }
}
