﻿using UnityEngine;

namespace Talespin.UI
{
    public class ProjScreen : MonoBehaviour
    {
        public ScreenType screenType;

        protected virtual void Awake()
        {
            UIManager.Instance.AddScreen(this);
            this.ExitScreen();
        }

        public virtual void EnterScreen()
        {
            this.gameObject.SetActive(true);
        }

        public virtual void ExitScreen()
        {
            this.gameObject.SetActive(false);
        }
    }
}
