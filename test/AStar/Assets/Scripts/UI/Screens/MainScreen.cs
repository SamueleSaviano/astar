﻿using UnityEngine;
using UnityEngine.UI;
using Talespin.UI;

public class MainScreen : ProjScreen
{
    [SerializeField]
    private GameObject _mapButton;
    [SerializeField]
    private GameObject _pathButton;
    [SerializeField]
    private GameObject _resetButton;
    [SerializeField]
    private Text _instructions;

    private StringData _data => ProjectManager.Instance.stringData;

    public override void EnterScreen()
    {
        base.EnterScreen();
        this.CheckElements(false);
    }

    public override void ExitScreen()
    {
        base.ExitScreen();
    }

    public void CheckElements(bool p_searching)
    {
        this._mapButton.SetActive(ProjectManager.Path == null && ProjectManager.StartNode == null && ProjectManager.TargetNode == null);
        this._pathButton.SetActive(ProjectManager.TargetNode != null && ProjectManager.Path == null);
        this._resetButton.SetActive(ProjectManager.Path != null);

        if(ProjectManager.StartNode == null)
        {
            this._instructions.text = this._data.startTileTip;
        }
        else
        {
            if(ProjectManager.TargetNode == null)
            {
                this._instructions.text = this._data.targetTileTip;
            }
            else
            {
                if(!ProjectManager.PathFound)
                {
                    this._instructions.text = p_searching ? this._data.noPathFound : this._data.pathTip;
                }
                else
                {
                    this._instructions.text = this._data.resetTip;
                }
            }
        }
    }

    public void MapButton()
    {
        Talespin.Generic.Grid.Instance.ResetGrid();
    }

    public void PathButton()
    {
        ProjectManager.Instance.GetPath();
        this.CheckElements(true);
    }

    public void ResetButton()
    {
        ProjectManager.Instance.ResetPath();
        this.CheckElements(false);
    }
}
