﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ProjectData", menuName = "Data/Project Data")]
public class ProjectData : ScriptableObject
{
    public List<ScriptableObject> data = new List<ScriptableObject>();

    public T GetData<T>() where T : ScriptableObject
    {
        return this.data.Find(_x => _x.GetType() == typeof(T)) as T;
    }
}
