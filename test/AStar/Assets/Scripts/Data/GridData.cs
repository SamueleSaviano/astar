﻿using UnityEngine;

[CreateAssetMenu(fileName = "GridData", menuName = "Data/Grid Data")]
public class GridData : ScriptableObject
{
    public int rows;
    public int columns;
    public float pathHighlightFrequency;
}
