﻿using UnityEngine;

[CreateAssetMenu(fileName = "StringData", menuName = "Data/String Data")]
public class StringData : ScriptableObject
{
    public string startTileTip;
    public string targetTileTip;
    public string pathTip;
    public string resetTip;
    public string noPathFound;
}
