﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NodesData", menuName = "Data/Nodes Data")]
public class NodesData : ScriptableObject
{
    [Serializable]
    public struct NodeData
    {
        public NodeType type;
        public Texture texture;

        [Tooltip("A negative value means that the node can't be routed")]
        public float cost;
    }

    public GameObject prefab;
    public string tag;

    [Space]

    public Color selectionColor;
    public Color pathColor;

    [Space]

    public float selectionYOffset;

    [Space]

    public List<NodeData> nodesData = new List<NodeData>();

    public NodeData GetData(NodeType p_nodeType)
    {
        return this.nodesData.Find(_x => _x.type == p_nodeType);
    }

    public Texture GetTexture(NodeType p_nodeType)
    {
        return this.GetData(p_nodeType).texture;
    }

    public float GetCost(NodeType p_nodeType)
    {
        return this.GetData(p_nodeType).cost;
    }
}
