﻿public static class Extentions
{
    public static float Percentage(this float p_value, float p_percentage)
    {
        return p_value / 100f * p_percentage;

    }
}
