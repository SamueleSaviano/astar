﻿using System.Collections.Generic;
using Talespin.Generic;

namespace Pathing
{
    public static class BFS
    {
        private static Queue<Node> _Queue = new Queue<Node>();
        private static HashSet<Node> _Visited = new HashSet<Node>();
        private static List<Node> _Path = new List<Node>();
        private static Dictionary<Node, Node> _Previous = new Dictionary<Node, Node>();

        private static List<Node> _CostEstmationPath = new List<Node>();
        private static List<Node> _CostEstmationNodes = new List<Node>();
        private static Node _SameParentNode;

        public static Dictionary<Node, Node> Solve(Node p_start)
        {
            Node _node;

            BFS._Queue.Clear();
            BFS._Visited.Clear();
            BFS._Previous.Clear();

            BFS._Queue.Enqueue(p_start);

            while (_Queue.Count > 0)
            {
                _node = _Queue.Dequeue();

                if (BFS._Visited.Contains(_node))
                {
                    continue;
                }

                BFS._Visited.Add(_node);

                foreach (Node f_node in _node.Neighbours)
                {
                    if (!BFS._Visited.Contains(f_node))
                    {
                        BFS._Queue.Enqueue(f_node);
                        BFS._Previous[f_node] = _node;
                    }
                }
            }

            return BFS._Previous;
        }

        private static List<Node> GetPath(Node p_start, Node p_target, Dictionary<Node, Node> p_prev)
        {
            BFS._Path.Clear();
            Node _current = p_target;

            while (!_current.Equals(p_start))
            {
                BFS._Path.Add(_current);

                if (!p_prev.ContainsKey(_current))
                {
                    break;
                }

                _current = p_prev[_current];
            }

            BFS._Path.Add(p_start);
            BFS._Path.Reverse();

            return BFS._Path;
        }

        public static float EstimatedCost(Node p_start, Node p_target)
        {
            BFS._CostEstmationPath = BFS.GetPath(p_start, p_target, BFS.Solve(p_start));
            float _result = 0;

            foreach (Node f_node in BFS._CostEstmationPath)
            {
                if (BFS._CostEstmationNodes.Contains(f_node))
                {
                    continue;
                }

                if (BFS._Previous.ContainsKey(f_node))
                {
                    BFS._SameParentNode = BFS._CostEstmationNodes.Find(_x => BFS._Previous.ContainsKey(_x) && BFS._Previous[_x] == BFS._Previous[f_node]);

                    if (BFS._SameParentNode == null)
                    {
                        BFS._CostEstmationNodes.Add(f_node);
                    }
                    else
                    {
                        if (f_node.cost < BFS._SameParentNode.cost)
                        {
                            BFS._CostEstmationNodes.Remove(BFS._SameParentNode);
                            BFS._CostEstmationNodes.Add(f_node);
                        }
                    }
                }
                else
                {
                    BFS._CostEstmationNodes.Add(f_node);
                }
            }

            foreach (Node f_node in BFS._CostEstmationNodes)
            {
                _result += f_node.cost;
            }

            return _result;
        }
    }
}
