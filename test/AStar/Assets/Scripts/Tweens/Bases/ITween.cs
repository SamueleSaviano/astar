﻿namespace Talespin.Tween
{
    public interface ITween
    {
        void PlayForward();
        void PlayReverse();
        void Stop();
        void ResetToBeginning();
    }
}
