﻿using System;
using UnityEngine;

namespace Talespin.Tween
{
    public enum TweenDirection
    {
        Forward,
        Reverse
    }

    public enum LoopType
    {
        None,
        PingPong,
        Rewind
    }

    public enum DelayType
    {
        OnStart,
        OnLoopStep
    }

    public class Tweener<T> : MonoBehaviour, ITween
    {
        #region Public Parameters

        public AnimationCurve _curve;

        [Space]

        public LoopType loopType;
        public DelayType delayType;

        [Space]

        public float duration = 1f;
        public float delay = 0;

        public bool playOnAwake = true;
        public bool ignoreTimeScale = false;

        [Space]

        public T from;
        public T to;

        #endregion

        #region Private

        private Action _behaviour = null;

        protected (T from, T to) _currentDestinations;
        private TweenDirection _direction;

        protected float _evaluationValue;
        private float _delayTimer;

        protected bool _canPlay = false;

        protected float _deltaTime => this.ignoreTimeScale ? Mathf.Clamp(Time.unscaledDeltaTime, 0f, 0.33f) : Time.deltaTime;
        protected float _time => _deltaTime / this.duration;
        protected float _t => this._curve.Evaluate(this._evaluationValue);
        private bool loop => this.loopType != LoopType.None;
        
        #endregion

        public virtual void SetBehaviour(Action p_behaviour)
        {
            this._behaviour = p_behaviour;
        }

        public virtual void SetTween(T p_from, T p_to, float p_duration = -1f, bool p_setParameters = false)
        {
            if (p_duration >= 0)
            {
                this.duration = p_duration;
            }

            this._currentDestinations.from = p_from;
            this._currentDestinations.to = p_to;

            if (p_setParameters)
            {
                this.from = p_from;
                this.to = p_to;
            }
        }

        protected virtual void Reset()
        {
            Keyframe[] _keys = new Keyframe[2];

            _keys[0] = new Keyframe(0, 0);
            _keys[1] = new Keyframe(1f, 1f);

            this._curve = new AnimationCurve(_keys);

            this.loopType = LoopType.None;

            this.duration = 1f;
            this.delay = 0;

            this.playOnAwake = true;
            this.ignoreTimeScale = true;

            this.SetTween(this.from, this.to);
        }

        protected virtual void OnEnable()
        {
            this._evaluationValue = 0;

            if(this._behaviour == null)
            {
                Debug.LogError("Missing Behaviour");
                return;
            }

            if (this._curve != null && this.playOnAwake)
            {
                this.PlayForward();
            }
        }

        public virtual void PlayForward()
        {
            this.SetTween(this.from, this.to);
            this.Play(TweenDirection.Forward);
        }

        public virtual void PlayReverse()
        {
            this.SetTween(this.to, this.from);
            this.Play(TweenDirection.Reverse);
        }

        private void Play(TweenDirection p_direction, bool p_looping = false)
        {
            this._evaluationValue = 0;
            this._direction = p_direction;

            if (p_looping)
            {
                switch (this._direction)
                {
                    case TweenDirection.Forward:

                        this.SetTween(this.from, this.to);
                        break;

                    case TweenDirection.Reverse:

                        this.SetTween(this.to, this.from);
                        break;
                }

                if (this.delayType == DelayType.OnLoopStep)
                {
                    this._delayTimer = 0;
                }
            }
            else
            {
                this._delayTimer = 0;
            }

            this._canPlay = true;
        }

        public virtual void Stop()
        {
            this._canPlay = false;

            this._delayTimer = 0;
            this._evaluationValue = 0;
        }

        public virtual void ResetToBeginning()
        {
            this.Stop();
            this._evaluationValue = 0;
        }

        protected virtual void Update()
        {
            this.DoTween();
        }

        protected virtual void DoTween()
        {
            if (this._behaviour != null && 
                this._canPlay && 
                this._deltaTime < this.duration)
            {
                if (this.delay > 0 && this._delayTimer < this.delay)
                {
                    this._delayTimer += this._deltaTime;
                    return;
                }

                this._evaluationValue += this._time;
                this._behaviour.Invoke();

                if (this._t >= 1f)
                {
                    this.OnTweenFinish();
                }
            }
        }

        protected virtual void OnTweenFinish()
        {
            if (!this.loop)
            {
                this.Stop();
            }
            else
            {
                switch (this.loopType)
                {
                    case LoopType.PingPong:

                        this.Play(this._direction == TweenDirection.Forward ? TweenDirection.Reverse : TweenDirection.Forward, true);
                        break;

                    case LoopType.Rewind:

                        this.Play(this._direction, true);
                        break;

                    default:

                        this.Stop();
                        break;
                }
            }
        }
    }
}
