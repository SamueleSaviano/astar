﻿using System;
using UnityEngine;

namespace Talespin.Tween
{
    public class TweenPosition : Tweener<Vector3>
    {
        private void Awake()
        {
            this.SetBehaviour(new Action(() => 
            { 
                this.transform.localPosition = Vector3.Lerp(this._currentDestinations.from, this._currentDestinations.to, this._t); 
            }));
        }

        protected override void Reset()
        {
            this.from = this.transform.localPosition;
            this.to = this.transform.localPosition;

            base.Reset();
        }

        [ContextMenu("Play Forward")]
        public override void PlayForward()
        {
            base.PlayForward();
        }

        [ContextMenu("Play Reverse")]
        public override void PlayReverse()
        {
            base.PlayReverse();
        }

        [ContextMenu("Stop")]
        public override void Stop()
        {
            base.Stop();
        }

        private void ResetPosition()
        {
            this.transform.localPosition = this._currentDestinations.from;
        }

        public override void ResetToBeginning()
        {
            this.ResetPosition();
            base.ResetToBeginning();
        }
    }
}
