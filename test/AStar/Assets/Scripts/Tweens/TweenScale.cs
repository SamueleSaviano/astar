﻿using UnityEngine;

namespace Talespin.Tween
{
    public class TweenScale : Tweener<Vector3>
    {
        private void Awake()
        {
            this.SetBehaviour(new System.Action(() =>
            {
                this.transform.transform.localScale = Vector3.Lerp(this._currentDestinations.from, this._currentDestinations.to, this._t);
            }));
        }

        protected override void Reset()
        {
            this.from = this.transform.localScale;
            this.to = this.transform.localScale;

            base.Reset();
        }

        public override void ResetToBeginning()
        {
            this.transform.localScale = this.from;
            base.ResetToBeginning();
        }

        [ContextMenu("Play Forward")]
        public override void PlayForward()
        {
            base.PlayForward();
        }

        [ContextMenu("Play Reverse")]
        public override void PlayReverse()
        {
            base.PlayReverse();
        }

        [ContextMenu("Stop")]
        public override void Stop()
        {
            base.Stop();
        }

        private void ResetScale()
        {
            this.transform.localScale = this._currentDestinations.from;
        }
    }
}
