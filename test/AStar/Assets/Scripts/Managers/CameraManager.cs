﻿using UnityEngine;

public enum Corner
{
    BottomLeft,
    BottomRight,
    TopLeft,
    TopRight
}

public class CameraManager : MonoBehaviour
{
    public static CameraManager Instance;
    public static Camera MainCamera;

    public static float FrustumHeight;
    public static float FrustumWidth;

    private float _fov;

    [SerializeField]
    private float _desiredWidth;

    private void Awake()
    {
        CameraManager.Instance = this;
        CameraManager.MainCamera = Camera.main;

        this.GetScreenSize();
    }

    private void Start()
    {
        this.AsjustFOV();
    }

    private void GetScreenSize()
    {
        CameraManager.FrustumHeight = 2.0f * CameraManager.MainCamera.farClipPlane * Mathf.Tan(CameraManager.MainCamera.fieldOfView * 0.5f * Mathf.Deg2Rad);
        CameraManager.FrustumWidth = CameraManager.FrustumHeight * CameraManager.MainCamera.aspect;
    }

    [ContextMenu("Adjust FOV")]
    private void AsjustFOV()
    {
        this._fov = Mathf.Atan(this._desiredWidth / CameraManager.FrustumHeight) * Mathf.Rad2Deg * 2f;
        CameraManager.MainCamera.fieldOfView = this._fov;
    }
}
