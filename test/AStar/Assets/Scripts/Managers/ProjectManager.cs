﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Talespin.Generic;
using Pathing;

public enum PathMarginType
{
    Start,
    Target
}

public class ProjectManager : MonoBehaviour
{
    public static ProjectManager Instance;

    public static Node StartNode;
    public static Node TargetNode;

    public static IList<IAStarNode> Path;
    public static bool PathFound;

    #region Data

    public ProjectData projectData;
    public NodesData nodesData { get; private set; }
    public GridData gridData { get; private set; }
    public StringData stringData { get; private set; }

    #endregion

    #region Selection

    private Ray _ray;
    private RaycastHit _hit;

    #endregion

    private void Awake()
    {
        ProjectManager.Instance = this;

        this.nodesData = this.projectData.GetData<NodesData>();
        this.gridData = this.projectData.GetData<GridData>();
        this.stringData = this.projectData.GetData<StringData>();
    }

    private void Start()
    {
        Talespin.Generic.Grid.Instance.CreateGrid();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            this._ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(this._ray, out this._hit))
            {
                if (_hit.collider.CompareTag(this.nodesData.tag))
                {
                    _hit.collider.GetComponentInParent<Node>()?.OnSelection();
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            this.GetPath();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            this.ResetPath();
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            Talespin.Generic.Grid.Instance.ResetGrid();
        }
    }

    public void GetPath()
    {
        if (ProjectManager.StartNode && ProjectManager.TargetNode)
        {
            ProjectManager.Path = AStar.GetPath(ProjectManager.StartNode, ProjectManager.TargetNode);
            ProjectManager.PathFound = ProjectManager.Path != null;

            if (ProjectManager.PathFound)
            {
                StartCoroutine(this.HighlightPath());
            }
        }
    }

    private IEnumerator HighlightPath()
    {
        Node _node;

        for (int i = 0; i < ProjectManager.Path?.Count; i++)
        {
            _node = (Node)ProjectManager.Path[i];

            if (_node != ProjectManager.StartNode && _node != ProjectManager.TargetNode)
            {
                _node.ToggleHighlight(true, HighlightType.Path);

                if (ProjectManager.PathFound)
                {
                    yield return new WaitForSeconds(this.gridData.pathHighlightFrequency);
                }
                else
                {
                    break;
                }
            }
        }
    }

    public void ResetPath()
    {
        if (ProjectManager.PathFound)
        {
            ProjectManager.StartNode = null;
            ProjectManager.TargetNode = null;

            if (ProjectManager.Path.Count > 0)
            {
                foreach (IAStarNode f_node in ProjectManager.Path)
                {
                    ((Node)f_node).ToggleHighlight(false);
                }
            }

            ProjectManager.Path.Clear();
            ProjectManager.Path = null;

            ProjectManager.PathFound = false;
        }

    }

    public void CheckPathMargin(PathMarginType p_type, Node p_node, Action p_onMarginCheckFail = null)
    {
        Node _margin = p_type == PathMarginType.Start ? ProjectManager.StartNode : ProjectManager.TargetNode;
        bool _canDeselect = true;

        if (_margin == null)
        {
            _margin = p_node;
            p_node.ToggleHighlight(true, HighlightType.Selection);

        }
        else
        {
            if (_margin == p_node)
            {
                switch (p_type)
                {
                    case PathMarginType.Start:

                        _canDeselect = ProjectManager.TargetNode == null;
                        break;

                    case PathMarginType.Target:

                        _canDeselect = true;
                        break;
                }

                if (_canDeselect)
                {
                    _margin = null;
                    p_node.ToggleHighlight(false);
                }
            }
            else
            {
                p_onMarginCheckFail?.Invoke();
            }
        }

        switch (p_type)
        {
            case PathMarginType.Start:

                ProjectManager.StartNode = _margin;
                break;

            case PathMarginType.Target:

                ProjectManager.TargetNode = _margin;
                break;
        }
    }
}
