﻿using System.Collections.Generic;
using UnityEngine;
using Talespin.UI;

public enum ScreenType
{
    Intro,
    MainScreen
}

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;

    public static IEnumerable<ProjScreen> Screens => UIManager.Instance._screens;
    private List<ProjScreen> _screens = new List<ProjScreen>();

    public static ProjScreen CurrentScreen;

    public ProjScreen GetScreen(ScreenType p_screenType)
    {
        return this._screens.Find(_x => _x.screenType == p_screenType);
    }

    public T GetScreenScript<T>() where T : ProjScreen
    {
        return (T)this._screens.Find(_x => _x.GetType() == typeof(T));
    }

    private void Awake()
    {
        UIManager.Instance = this;
    }

    private void Start()
    {
        this.ChangeScreen(ScreenType.Intro);
    }

    public void AddScreen(ProjScreen p_screen)
    {
        this._screens.Add(p_screen);
    }

    public void ChangeScreen(ScreenType p_newScreen)
    {
        if (UIManager.CurrentScreen != null)
        {
            UIManager.CurrentScreen.ExitScreen();
        }

        UIManager.CurrentScreen = this.GetScreen(p_newScreen);
        UIManager.CurrentScreen.EnterScreen();
    }
}
